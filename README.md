
# KSP French Translate 1.12.x

A simple to way to translate your KSP game if you haven't access to the official translate.
Copy and past the folder and here we go ! 

If you see a mistake in game, please let me know and i will fix as soon as possible.

I tried to be as close as possible to the original technical meaning so if you liked my work,
give me a star and have fun while waiting to KSP2 !


## Installation

Install by drag-n-drop the "GameData" folder into the root directory of your KSP.
It will overwrite the original language so be careful if you want to rollback : make a backup !

```bash
  copy ksp-french-translate-1.12.x/
  paste GameData into %PATH%/Kerbal Space Program/

  then

  open "buildID64.txt" file and replace "en-us" by "fr-fr"
```
    
## License

[GNU](https://choosealicense.com/licenses/gpl-3.0//)
